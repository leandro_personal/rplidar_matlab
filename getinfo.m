function [info] = getinfo(s)

fopen(s);           %opens the serial port

fwrite(s,165);      %Envia 0xA5
fwrite(s,80);       %Envia 0x50
Response_Descriptor = fread(s , 7,'uint8');
Start_flag1 = Response_Descriptor(1);
Start_flag2 = Response_Descriptor(2);
if Start_flag1==165 && Start_flag2==90
    length = Response_Descriptor(3);
    Data_response = fread(s , length,'uint8');
    info.model = num2str(Data_response(1));
    info.fw_vesion = strcat(num2str(Data_response(3)),'.',num2str(Data_response(2)));
    info.hardware = num2str(Data_response(4));
    for i=1:16
        if i==1
           info.serial = byte2str(Data_response(4+i)); 
        end
       info.serial= strcat(info.serial,':',byte2str(Data_response(4+i))); 
    end
else
    fclose(s);
    info=0;
    return;
end
fclose(s);

end

function [str]= byte2str(x)
if x<0
    x = typecast(int8(x),'uint8');
end
str = num2str(x,'%x');
tempstr = flip(str);
while length(tempstr)~=2
    tempstr(end+1)='0';
end

str = flip(tempstr);
end