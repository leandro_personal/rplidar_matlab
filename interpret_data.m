data_points = floor(length(data_read)/5);
index=1;
for i=1:data_points
    data(i).startflag = bitget(data_read(index),1);
    data(i).not_startflag = bitget(data_read(index),2);
    data(i).quality = bitshift(data_read(index+1),-2);
    
    data(i).checkbit = bitget(data_read(index+1),1);
    
    angle(1) = data_read(index+1);
    angle(2) = data_read(index+2);
    data(i).angle = typecast(uint8(angle(1:2)), 'uint16');
    data(i).angle = bitshift(data(i).angle,-1);
    data(i).angle = double(data(i).angle)/64;
    rho(i) = deg2rad(data(i).angle);
    distance(1) = data_read(index+3);
    distance(2) = data_read(index+4);
    data(i).distance = typecast(uint8(distance(1:2)), 'uint16');
    data(i).distance = double(data(i).distance)/4;
    theta(i) = data(i).distance;
    index = index +5;
end