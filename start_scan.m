function start_scan(s)

set(s,'BytesAvailableFcnMode','byte');
set(s,'BytesAvailableFcnCount',360*5);
set(s,'BytesAvailableFcn', @serial_callback);

fopen(s);

fwrite(s,165);      %Envia 0xA5
fwrite(s,37);       %Envia 0x25
pause(1);

fwrite(s,165);      %Envia 0xA5
fwrite(s,32);       %Envia 0x20

end

function serial_callback(obj,event)
% fwrite(obj,165);      %Envia 0xA5
% fwrite(obj,37);       %Envia 0x25


% Response_Descriptor = fread(obj , 7,'uint8');
% Start_flag1 = Response_Descriptor(1);
% Start_flag2 = Response_Descriptor(2);
% if Start_flag1==165 && Start_flag2==90
%for i=1:360
data_read = fread(obj , obj.BytesAvailable,'uint8');
if exist('Lidar.mat','file')==0
    save('Lidar','data_read')
end
%end
% end
% fclose(obj);
end