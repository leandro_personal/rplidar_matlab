function [string_text]=reset_lidar(s)
%Est� fun��o considera que a porta serial j� est� aberta
if strcmp(s.status,'closed')
    fopen(s);
end
fwrite(s,165);      %Envia 0xA5
fwrite(s,64);       %Envia 0x25
pause(1);
length= s.BytesAvailable;
data_read = fread(s , s.BytesAvailable,'char');
for i=1:length
    if i==1
        string_text = char(data_read(i));
    else
        if data_read(i) == 32
            string_text = strcat(string_text,'_');
        else
            string_text = strcat(string_text,data_read(i));
        end
    end
end
fclose(s);

end