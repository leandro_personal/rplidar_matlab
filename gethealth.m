function [health] = gethealth(s)

fopen(s);           %opens the serial port

fwrite(s,165);      %Envia 0xA5
fwrite(s,82);       %Envia 0x52
Response_Descriptor = fread(s , 7,'uint8');
Start_flag1 = Response_Descriptor(1);
Start_flag2 = Response_Descriptor(2);
if Start_flag1==165 && Start_flag2==90
    length = Response_Descriptor(3);
    Data_response = fread(s , length,'uint8');
    if Data_response(1)==0
      health.status ='Good';  
    elseif Data_response(1)==1
      health.status ='Warning';   
    elseif Data_response(1)==2
      health.status ='Error';   
    end
    health.error_code = typecast(uint8(Data_response(2:3)), 'uint16');
else
    fclose(s);
    health=0;
    return;
end
fclose(s);

end