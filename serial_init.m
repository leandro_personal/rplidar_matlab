function [s] = serial_init()
clc;
clear;
instrreset;
COM_list = getAvailableComPort()

COM= input('Escolha a COM para conectar com o Lidar ');
COM = strcat('COM',num2str(COM));

s = serial(COM); %assigns the object s to serial port

set(s, 'InputBufferSize', 5000); %number of bytes in inout buffer 3084
set(s, 'FlowControl', 'none');
set(s, 'BaudRate', 115200);
set(s, 'Parity', 'none');
set(s, 'DataBits', 8);
set(s, 'StopBit', 1);
set(s, 'Timeout',10);
set(s,'DataTerminalReady', 'off');

end